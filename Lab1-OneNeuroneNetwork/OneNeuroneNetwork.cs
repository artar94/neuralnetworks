﻿using System.Collections.Generic;
using System.Linq;
using NeuralNetwork;

namespace OneNeurone
{
	public sealed class OneNeuroneNetwork: NeuralNetwork.NeuralNetwork
	{
		public override int NeuroneCount
		{
			get { return Neurones.Count; }
			set
			{
				if (Neurones == null)
					Neurones = new List<Neurone>(1);

				if (Neurones.Count < 1)
					Neurones.Add(new Neurone());

				Neurones.RemoveRange(1, Neurones.Count - 1);
			}
		}

		public override IEnumerable<double> Recognize(ISynapse<double>[] synapses)
		{
			Neurones.First().Synapses = synapses;
			return new[] { Neurones.First().Recognize()};
		}

		public override void Learn(ISynapse<double>[] synapses, IList<double> answers)
		{
			if (!answers.Any())
				return;

			Neurones.First().Synapses = synapses;
			Neurones.First().Learn(answers.First());

			OnTrained();
		}
	}
}