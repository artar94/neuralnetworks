﻿using NeuralNetwork.Ui;

namespace OneNeurone
{
	partial class OneNeuroneWindow
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OneNeuroneWindow));
			this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
			this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
			this.mainMenu = new System.Windows.Forms.MainMenu(this.components);
			this.menuItemFile = new System.Windows.Forms.MenuItem();
			this.menuItemNew = new System.Windows.Forms.MenuItem();
			this.menuItemSeperator1 = new System.Windows.Forms.MenuItem();
			this.menuItemOpen = new System.Windows.Forms.MenuItem();
			this.menuItemSave = new System.Windows.Forms.MenuItem();
			this.menuItemSaveAs = new System.Windows.Forms.MenuItem();
			this.menuItemSeparator2 = new System.Windows.Forms.MenuItem();
			this.menuItemExit = new System.Windows.Forms.MenuItem();
			this.menuItemAbout = new System.Windows.Forms.MenuItem();
			this.panel2 = new System.Windows.Forms.Panel();
			this.neuroneMonitor = new NeuralNetwork.Ui.NeuroneMonitor();
			this.panel1 = new System.Windows.Forms.Panel();
			this.labelRes = new System.Windows.Forms.Label();
			this.labelResult = new System.Windows.Forms.Label();
			this.synapseSensor = new NeuralNetwork.Ui.SynapseSensor();
			this.buttonLearn = new System.Windows.Forms.Button();
			this.buttonClear = new System.Windows.Forms.Button();
			this.buttonRecognize = new System.Windows.Forms.Button();
			this.panel2.SuspendLayout();
			this.panel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.synapseSensor)).BeginInit();
			this.SuspendLayout();
			// 
			// saveFileDialog
			// 
			this.saveFileDialog.DefaultExt = "nmem";
			this.saveFileDialog.FileName = "Lab1 - Neurone Memory";
			this.saveFileDialog.Filter = "Память нейрона|*.nmem|Все файлы|*.*";
			// 
			// openFileDialog
			// 
			this.openFileDialog.Filter = "Память нейрона|*.nmem|Все файлы|*.*";
			// 
			// mainMenu
			// 
			this.mainMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItemFile,
            this.menuItemAbout});
			// 
			// menuItemFile
			// 
			this.menuItemFile.Index = 0;
			this.menuItemFile.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItemNew,
            this.menuItemSeperator1,
            this.menuItemOpen,
            this.menuItemSave,
            this.menuItemSaveAs,
            this.menuItemSeparator2,
            this.menuItemExit});
			this.menuItemFile.Text = "&Файл";
			// 
			// menuItemNew
			// 
			this.menuItemNew.Index = 0;
			this.menuItemNew.Shortcut = System.Windows.Forms.Shortcut.CtrlN;
			this.menuItemNew.Text = "&Новый";
			this.menuItemNew.Click += new System.EventHandler(this.menuItemNew_Click);
			// 
			// menuItemSeperator1
			// 
			this.menuItemSeperator1.Index = 1;
			this.menuItemSeperator1.Text = "-";
			// 
			// menuItemOpen
			// 
			this.menuItemOpen.Index = 2;
			this.menuItemOpen.Shortcut = System.Windows.Forms.Shortcut.CtrlO;
			this.menuItemOpen.Text = "&Открыть";
			this.menuItemOpen.Click += new System.EventHandler(this.menuItemOpen_Click);
			// 
			// menuItemSave
			// 
			this.menuItemSave.Index = 3;
			this.menuItemSave.Shortcut = System.Windows.Forms.Shortcut.CtrlS;
			this.menuItemSave.Text = "&Сохранить";
			this.menuItemSave.Click += new System.EventHandler(this.menuItemSave_Click);
			// 
			// menuItemSaveAs
			// 
			this.menuItemSaveAs.Index = 4;
			this.menuItemSaveAs.Shortcut = System.Windows.Forms.Shortcut.CtrlShiftS;
			this.menuItemSaveAs.Text = "Сохранить &как";
			this.menuItemSaveAs.Click += new System.EventHandler(this.menuItemSaveAs_Click);
			// 
			// menuItemSeparator2
			// 
			this.menuItemSeparator2.Index = 5;
			this.menuItemSeparator2.Text = "-";
			// 
			// menuItemExit
			// 
			this.menuItemExit.Index = 6;
			this.menuItemExit.Shortcut = System.Windows.Forms.Shortcut.AltF4;
			this.menuItemExit.Text = "В&ыход";
			this.menuItemExit.Click += new System.EventHandler(this.menuItemExit_Click);
			// 
			// menuItemAbout
			// 
			this.menuItemAbout.Index = 1;
			this.menuItemAbout.Text = "&О программе";
			this.menuItemAbout.Click += new System.EventHandler(this.menuItemAbout_Click);
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.neuroneMonitor);
			this.panel2.Controls.Add(this.panel1);
			this.panel2.Controls.Add(this.synapseSensor);
			this.panel2.Controls.Add(this.buttonLearn);
			this.panel2.Controls.Add(this.buttonClear);
			this.panel2.Controls.Add(this.buttonRecognize);
			this.panel2.Location = new System.Drawing.Point(4, 0);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(303, 156);
			this.panel2.TabIndex = 10;
			// 
			// neuroneMonitor
			// 
			this.neuroneMonitor.Location = new System.Drawing.Point(205, 3);
			this.neuroneMonitor.Name = "neuroneMonitor";
			this.neuroneMonitor.Neurone = null;
			this.neuroneMonitor.PixelSize = 1;
			this.neuroneMonitor.Size = new System.Drawing.Size(96, 121);
			this.neuroneMonitor.TabIndex = 15;
			// 
			// panel1
			// 
			this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.panel1.Controls.Add(this.labelRes);
			this.panel1.Controls.Add(this.labelResult);
			this.panel1.Location = new System.Drawing.Point(3, 104);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(196, 49);
			this.panel1.TabIndex = 14;
			// 
			// labelRes
			// 
			this.labelRes.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
			this.labelRes.Location = new System.Drawing.Point(2, 2);
			this.labelRes.Name = "labelRes";
			this.labelRes.Size = new System.Drawing.Size(91, 43);
			this.labelRes.TabIndex = 7;
			this.labelRes.Text = "Результат:";
			this.labelRes.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// labelResult
			// 
			this.labelResult.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.labelResult.Font = new System.Drawing.Font("Segoe UI", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.labelResult.Location = new System.Drawing.Point(98, 2);
			this.labelResult.Name = "labelResult";
			this.labelResult.Size = new System.Drawing.Size(94, 43);
			this.labelResult.TabIndex = 6;
			this.labelResult.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// synapseSensor
			// 
			this.synapseSensor.BackColor = System.Drawing.Color.White;
			this.synapseSensor.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("synapseSensor.BackgroundImage")));
			this.synapseSensor.BlackConst = 1D;
			this.synapseSensor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.synapseSensor.BrushColor = System.Drawing.Color.Black;
			this.synapseSensor.ClearBrushWidth = 5;
			this.synapseSensor.Image = ((System.Drawing.Image)(resources.GetObject("synapseSensor.Image")));
			this.synapseSensor.Location = new System.Drawing.Point(3, 3);
			this.synapseSensor.Name = "synapseSensor";
			this.synapseSensor.PaintBrushWidth = 3;
			this.synapseSensor.Painting = true;
			this.synapseSensor.PaintOverGrid = true;
			this.synapseSensor.PixelGridColor = System.Drawing.Color.Gainsboro;
			this.synapseSensor.PixelSize = 1;
			this.synapseSensor.Size = new System.Drawing.Size(95, 95);
			this.synapseSensor.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
			this.synapseSensor.TabIndex = 13;
			this.synapseSensor.TabStop = false;
			this.synapseSensor.WhiteConst = 0D;
			// 
			// buttonLearn
			// 
			this.buttonLearn.Location = new System.Drawing.Point(104, 69);
			this.buttonLearn.Name = "buttonLearn";
			this.buttonLearn.Size = new System.Drawing.Size(95, 27);
			this.buttonLearn.TabIndex = 12;
			this.buttonLearn.Text = "Обучить";
			this.buttonLearn.UseVisualStyleBackColor = true;
			this.buttonLearn.Visible = false;
			this.buttonLearn.Click += new System.EventHandler(this.buttonLearn_Click);
			// 
			// buttonClear
			// 
			this.buttonClear.Location = new System.Drawing.Point(104, 36);
			this.buttonClear.Name = "buttonClear";
			this.buttonClear.Size = new System.Drawing.Size(95, 27);
			this.buttonClear.TabIndex = 11;
			this.buttonClear.Text = "Очистить";
			this.buttonClear.UseVisualStyleBackColor = true;
			this.buttonClear.Click += new System.EventHandler(this.buttonClear_Click);
			// 
			// buttonRecognize
			// 
			this.buttonRecognize.Location = new System.Drawing.Point(104, 3);
			this.buttonRecognize.Name = "buttonRecognize";
			this.buttonRecognize.Size = new System.Drawing.Size(95, 27);
			this.buttonRecognize.TabIndex = 10;
			this.buttonRecognize.Text = "Распознать";
			this.buttonRecognize.UseVisualStyleBackColor = true;
			this.buttonRecognize.Click += new System.EventHandler(this.buttonRecognize_Click);
			// 
			// OneNeuroneWindow
			// 
			this.AllowDrop = true;
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.AutoSize = true;
			this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.ClientSize = new System.Drawing.Size(322, 178);
			this.Controls.Add(this.panel2);
			this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.KeyPreview = true;
			this.MaximizeBox = false;
			this.Menu = this.mainMenu;
			this.Name = "OneNeuroneWindow";
			this.Text = "Перцептрон";
			this.Load += new System.EventHandler(this.MainWindow_Load);
			this.DragDrop += new System.Windows.Forms.DragEventHandler(this.MainWindow_DragDrop);
			this.DragEnter += new System.Windows.Forms.DragEventHandler(this.MainWindow_DragEnter);
			this.panel2.ResumeLayout(false);
			this.panel1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.synapseSensor)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion
		private System.Windows.Forms.SaveFileDialog saveFileDialog;
		private System.Windows.Forms.OpenFileDialog openFileDialog;
		private System.Windows.Forms.MainMenu mainMenu;
		private System.Windows.Forms.MenuItem menuItemFile;
		private System.Windows.Forms.MenuItem menuItemNew;
		private System.Windows.Forms.MenuItem menuItemOpen;
		private System.Windows.Forms.MenuItem menuItemSaveAs;
		private System.Windows.Forms.MenuItem menuItemExit;
		private System.Windows.Forms.MenuItem menuItemSeperator1;
		private System.Windows.Forms.MenuItem menuItemSeparator2;
		private System.Windows.Forms.Panel panel2;
		private NeuroneMonitor neuroneMonitor;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Label labelRes;
		private System.Windows.Forms.Label labelResult;
		private SynapseSensor synapseSensor;
		private System.Windows.Forms.Button buttonLearn;
		private System.Windows.Forms.Button buttonClear;
		private System.Windows.Forms.Button buttonRecognize;
		private System.Windows.Forms.MenuItem menuItemSave;
		private System.Windows.Forms.MenuItem menuItemAbout;
	}
}

