﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using NeuralNetwork;
using NeuralNetwork.Ui;

namespace OneNeurone
{
	public enum Symbol
	{
		[Description("0")]
		Zero,

		[Description("8")]
		Eight,
	}

	public class Document: BaseDocument
	{
		public OneNeuroneNetwork NeuralNetwork { get; }

		public Document()
		{
			NeuralNetwork = new OneNeuroneNetwork
			{
				NeuroneCount = 1,
			};
		}

		public Document(string fileName) : this()
		{
			var trainer = new JsonTrainer<OneNeuroneNetwork>();
			NeuralNetwork = trainer.LoadFromFile(fileName);
			NeuralNetwork.NeuroneCount = 1;

			FileName = fileName;
		}

		public override bool IsModified => false;

		public override void SaveToFile(string fileName)
		{
			var trainer = new JsonTrainer<OneNeuroneNetwork>();
			trainer.SaveToFile(fileName, NeuralNetwork);

			base.SaveToFile(fileName);
		}

		private static Symbol DoubleToSymbol(double value)
		{
			return value >= 0 ? Symbol.Zero : Symbol.Eight;
		}

		private static double SymbolToDouble(Symbol value)
		{
			return value == Symbol.Zero ? 1.0 : -1.0;
		}

		public Symbol Recognize(ISynapse<double>[] synapses)
		{
			return DoubleToSymbol(NeuralNetwork.Recognize(synapses).First());
		}

		public void Learn(ISynapse<double>[] synapses, Symbol symbol)
		{
			NeuralNetwork.Learn(synapses, new[] {SymbolToDouble(symbol)});
		}
	}
}