﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NeuralNetwork.Extensions;

namespace OneNeurone
{
	public partial class OneNeuroneWindow : Form
	{
		private Document _document;
		public Document Document
		{
			get
			{
				return _document;
			}
			set
			{
				_document = value;
				
				_document.Saved += DocumentOnSaved;

				_document.NeuralNetwork.Neurones.ForEach(n => n.Synapses = synapseSensor.Synapses);
				neuroneMonitor.PictureBoxWeights.ClientSize = synapseSensor.ClientSize;
				neuroneMonitor.Neurone = _document.NeuralNetwork.Neurones.First();

				synapseSensor.Clean();
				labelResult.Text = "";

				buttonLearn.Visible = false;

				UpdateCaption();
			}
		}

		private Symbol _recognizeSymbol;
		private Symbol RecognizeSymbol
		{
			get { return _recognizeSymbol; }
			set
			{
				_recognizeSymbol = value;
				labelResult.Text = _recognizeSymbol.GetDescription();
				buttonLearn.Text = $"Это не {_recognizeSymbol.GetDescription()}";
			}
		}

		public OneNeuroneWindow()
		{
			InitializeComponent();
		}

		private void MainWindow_Load(object sender, EventArgs e)
		{
			Document = new Document();
		}

		private void DocumentOnSaved(object sender, string fileName)
		{
			UpdateCaption();
		}

		private void UpdateCaption()
		{
			var builder = new StringBuilder();
			if (Document.FileName != "")
				builder.Append($"{Path.GetFileNameWithoutExtension(Document.FileName)} - ");
			builder.Append(AboutBox.AssemblyTitle);
			Text = builder.ToString();
		}

		private bool SaveAs()
		{
			if (saveFileDialog.ShowDialog(this) != DialogResult.OK) return false;
			var fileName = saveFileDialog.FileName;

			try
			{
				Document.SaveToFile(fileName);
				return true;
			}
			catch (Exception exception)
			{
				MessageBox.Show(this, $"Не удаётся сохранить файл \"{fileName}\".\n{exception.Message}",
					AboutBox.AssemblyTitle,
					MessageBoxButtons.OK, MessageBoxIcon.Error);
				return false;
			}
		}

		private bool Save()
		{
			if (Document.FileName == "") return SaveAs();

			try
			{
				Document.SaveToFile(Document.FileName);
				return true;
			}
			catch
			{
				return SaveAs();
			}
		}
		public bool IsReplacementPossible()
		{
			if (!Document.IsModified) return true;

			// ReSharper disable once SwitchStatementMissingSomeCases
			switch (MessageBox.Show(this, @"Документ был изменён. Сохранить?", AboutBox.AssemblyTitle,
				MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning))
			{
				case DialogResult.Cancel:
					return false;
				case DialogResult.Yes:
					return Save();
				default:
					return true;
			}
		}

		private void buttonClear_Click(object sender, EventArgs e)
		{
			synapseSensor.Clean();
			labelResult.Text = "";

			buttonLearn.Visible = false;
		}

		private void buttonRecognize_Click(object sender, EventArgs e)
		{
			synapseSensor.Stretch();
			RecognizeSymbol = Document.Recognize(synapseSensor.Synapses);
			buttonLearn.Visible = true;
		}

		private void buttonLearn_Click(object sender, EventArgs e)
		{
			Document.Learn(synapseSensor.Synapses, RecognizeSymbol == Symbol.Zero ? Symbol.Eight : Symbol.Zero);
			RecognizeSymbol = Document.Recognize(synapseSensor.Synapses);
			buttonLearn.Visible = false;
		}

		private void menuItemNew_Click(object sender, EventArgs e)
		{
			if (!IsReplacementPossible()) return;
			Document = new Document();
		}

		private void menuItemOpen_Click(object sender, EventArgs e)
		{
			if (!IsReplacementPossible()) return;

			if (openFileDialog.ShowDialog(this) != DialogResult.OK) return;
			var fileName = openFileDialog.FileName;

			try
			{
				Document = new Document(fileName);
			}
			catch (Exception exception)
			{
				MessageBox.Show(this, $"Не удаётся открыть файл \"{fileName}\".\n{exception.Message}",
					AboutBox.AssemblyTitle,
					MessageBoxButtons.OK, MessageBoxIcon.Error);
			}

			buttonLearn.Visible = false;
		}

		private void menuItemSave_Click(object sender, EventArgs e)
		{
			Save();
		}

		private void menuItemSaveAs_Click(object sender, EventArgs e)
		{
			SaveAs();
		}

		private void menuItemExit_Click(object sender, EventArgs e)
		{
			Close();
		}

		private void menuItemAbout_Click(object sender, EventArgs e)
		{
			new AboutBox().ShowDialog(this);
		}

		private void MainWindow_DragDrop(object sender, DragEventArgs e)
		{
			var files = (string[])e.Data.GetData(DataFormats.FileDrop);
			if (files.Length <= 0) return;

			Activate();

			if (!IsReplacementPossible()) return;
			var fileName = files[0];
			try
			{
				Document = new Document(fileName);
			}
			catch (Exception exception)
			{
				MessageBox.Show(this, $"Не удаётся открыть файл \"{fileName}\".\n{exception.Message}",
					AboutBox.AssemblyTitle,
					MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}

		private void MainWindow_DragEnter(object sender, DragEventArgs e)
		{
			if (e.Data.GetDataPresent(DataFormats.FileDrop))
				e.Effect = DragDropEffects.Copy;
		}
	}
}
