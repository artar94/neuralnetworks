﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using NeuralNetwork;
using NeuralNetwork.Ui;

namespace HopfieldNetwork
{
	public partial class HopfieldWindow : Form
	{
		private bool _recognizing;
		private bool Recognizing
		{
			get { return _recognizing; }
			set
			{
				_recognizing = value;
				panelButtons.Enabled = !_recognizing;
				synapseSensor.Painting = !_recognizing;
				buttonRecognize.Text = _recognizing ? @"Отменить" : @"Восстановить";
			}
		}

		private Document _document;
		public Document Document
		{
			get
			{
				return _document;
			}
			set
			{
				Recognizing = false;

				_document = value;
				_document.Saved += DocumentOnSaved;

				SampleImages.ForEach(s =>
				{
					s.Parent = null;
					s.Dispose();
				});
				SampleImages.Clear();
				foreach (var sample in Document.NeuralNetwork.Samples)
				{
					AddSample(sample.Select(d => (ISynapse<double>) new Synapse(d)).ToArray());
				}
				if (SampleImages.Any())
					panelImages.ScrollControlIntoView(SampleImages.First());

				var synapses = synapseSensor.Synapses;
				Document.NeuralNetwork.Trained += sender => PlotEnergy();
				Document.NeuralNetwork.NeuroneCount = synapses.Length;
				Document.NeuralNetwork.Neurones.ForEach(n => n.Synapses = synapses);

				synapseSensor.Clean();

				UpdateCaption();
				PlotEnergy();
			}
		}

		private List<SynapseSensor> SampleImages { get; } = new List<SynapseSensor>();

		public HopfieldWindow()
		{
			InitializeComponent();
		}

		private void HopfieldWindow_Load(object sender, EventArgs e)
		{
			Document = new Document();
			panelImages.Height = synapseSensor.Height + SystemInformation.HorizontalScrollBarHeight + 2;
			Height = panelImages.Top + panelImages.Height + Height - panelMain.Height;
		}

		private void DocumentOnSaved(object sender, string fileName)
		{
			UpdateCaption();
		}

		private void UpdateCaption()
		{
			var builder = new StringBuilder();
			if (Document.FileName != "")
				builder.Append($"{Path.GetFileNameWithoutExtension(Document.FileName)} - ");
			builder.Append(AboutBox.AssemblyTitle);
			Text = builder.ToString();
		}

		private SynapseSensor AddSample()
		{
			var sensor = new SynapseSensor
			{
				BackColor = Color.White,
				BrushColor = synapseSensor.BrushColor,
				PixelSize = synapseSensor.PixelSize,
				ClearBrushWidth = synapseSensor.ClearBrushWidth,
				PaintBrushWidth = synapseSensor.PaintBrushWidth,
				PixelGridColor = synapseSensor.PixelGridColor,
				Painting = false,
				PaintOverGrid = synapseSensor.PaintOverGrid,
				BorderStyle = synapseSensor.BorderStyle,
				SizeMode = synapseSensor.SizeMode,
				Width = synapseSensor.Width,
				Height = synapseSensor.Height,
				Parent = panelImages,
				BlackConst = synapseSensor.BlackConst,
				WhiteConst = synapseSensor.WhiteConst,
			};
			sensor.DoubleClick += SampleOnDoubleClick;

			SampleImages.Add(sensor);
			SortSamples();
			panelImages.ScrollControlIntoView(SampleImages.Last());

			return sensor;
		}

		private SynapseSensor AddSample(ISynapse<double>[] synapses)
		{
			var sensor = AddSample();
			sensor.Synapses = synapses;
			return sensor;
		}

		private SynapseSensor AddSample(Image image)
		{
			var sensor = AddSample();
			sensor.Rasterization(image);
			return sensor;
		}

		private void SortSamples()
		{
			var left = 0;
			for (var i = 0; i < SampleImages.Count; i++)
			{
				SampleImages[i].Left = left - panelImages.HorizontalScroll.Value;
				left += SampleImages[i].Width;

				if (i != SampleImages.Count - 1)
					left += 5;
			}
		}

		private bool SaveAs()
		{
			if (saveFileDialog.ShowDialog(this) != DialogResult.OK) return false;
			var fileName = saveFileDialog.FileName;

			try
			{
				Document.SaveToFile(fileName);
				return true;
			}
			catch (Exception exception)
			{
				MessageBox.Show(this, $"Не удаётся сохранить файл \"{fileName}\".\n{exception.Message}",
					AboutBox.AssemblyTitle,
					MessageBoxButtons.OK, MessageBoxIcon.Error);
				return false;
			}
		}

		private bool Save()
		{
			if (Document.FileName == "") return SaveAs();

			try
			{
				Document.SaveToFile(Document.FileName);
				return true;
			}
			catch
			{
				return SaveAs();
			}
		}

		private void Open()
		{
			if (!IsReplacementPossible()) return;

			if (openFileDialog.ShowDialog(this) != DialogResult.OK) return;
			var fileName = openFileDialog.FileName;

			try
			{
				Document = new Document(fileName);
			}
			catch (Exception exception)
			{
				MessageBox.Show(this, $"Не удаётся открыть файл \"{fileName}\".\n{exception.Message}",
					AboutBox.AssemblyTitle,
					MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}

		public bool IsReplacementPossible()
		{
			if (!Document.IsModified) return true;

			// ReSharper disable once SwitchStatementMissingSomeCases
			switch (MessageBox.Show(this, @"Документ был изменён. Сохранить?", AboutBox.AssemblyTitle,
				MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning))
			{
				case DialogResult.Cancel:
					return false;
				case DialogResult.Yes:
					return Save();
				default:
					return true;
			}
		}

		private async Task Recognize()
		{
			Recognizing = true;

			var synapses = synapseSensor.Synapses;
			foreach (var step in Document.NeuralNetwork.Recovering(synapses))
			{
				if (!Recognizing)
					break;

				await Task.Factory.StartNew(() =>
				{
					synapseSensor.Synapses = step;
					Thread.Sleep(1);
				});
			}
			Recognizing = false;
		}

		private void Forget()
		{
			Document.Forget(synapseSensor.Synapses);
		}

		private async void buttonRecognize_Click(object sender, EventArgs e)
		{
			if (Recognizing)
				Recognizing = false;
			else
				await Recognize();
		}

		private void buttonClear_Click(object sender, EventArgs e)
		{
			synapseSensor.Clean();
		}

		private void buttonLearn_Click(object sender, EventArgs e)
		{
			var synapses = synapseSensor.Synapses;
			
			AddSample(synapses);
			Document.NeuralNetwork.Samples.Add(synapses.Select(s => s.Value).ToArray());
			Document.Learn(synapses);
		}

		private void buttonForget_Click(object sender, EventArgs e)
		{
			Forget();
		}

		private void buttonLoadImage_Click(object sender, EventArgs e)
		{
			if (openImageDialog.ShowDialog(this) != DialogResult.OK) return;
			var fileNames = openImageDialog.FileNames;
			foreach (var fileName in fileNames)
			{
				var sensor = AddSample(Image.FromFile(fileName));

				var synapses = sensor.Synapses;
				Document.NeuralNetwork.Samples.Add(synapses.Select(s => s.Value).ToArray());
				Document.Learn(synapses);
			}
		}

		private void PlotEnergy()
		{
			var points = chart.Series["Energy"].Points;
			points.Clear();
			
			for (var i = 0; i < Document.NeuralNetwork.NeuroneCount; i++)
			{
				var weight = 0.0;
				for (var j = 0; j < Document.NeuralNetwork.NeuroneCount; j++)
				{
					weight += Document.NeuralNetwork.Neurones[i].SynapseWeights[j];
				}
				points.AddY(weight);
			}
		}

		private void SampleOnDoubleClick(object sender, EventArgs eventArgs)
		{
			synapseSensor.Synapses = ((SynapseSensor) sender).Synapses;
		}

		private void menuItemNew_Click(object sender, EventArgs e)
		{
			if (!IsReplacementPossible()) return;
			Document = new Document();
		}

		private void menuItemOpen_Click(object sender, EventArgs e)
		{
			Open();
		}

		private void menuItemSave_Click(object sender, EventArgs e)
		{
			Save();
		}

		private void menuItemSaveAs_Click(object sender, EventArgs e)
		{
			SaveAs();
		}

		private void menuItemExit_Click(object sender, EventArgs e)
		{
			Close();
		}

		private void menuItemAbout_Click(object sender, EventArgs e)
		{
			new AboutBox().ShowDialog(this);
		}

		private void HopfieldWindow_DragDrop(object sender, DragEventArgs e)
		{
			var files = (string[])e.Data.GetData(DataFormats.FileDrop);
			if (files.Length <= 0) return;

			Activate();

			if (!IsReplacementPossible()) return;
			var fileName = files[0];
			try
			{
				Document = new Document(fileName);
			}
			catch (Exception exception)
			{
				MessageBox.Show(this, $"Не удаётся открыть файл \"{fileName}\".\n{exception.Message}",
					AboutBox.AssemblyTitle,
					MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}

		private void HopfieldWindow_DragEnter(object sender, DragEventArgs e)
		{
			if (e.Data.GetDataPresent(DataFormats.FileDrop))
				e.Effect = DragDropEffects.Copy;
		}

		private void synapseSensor_PaintEnd(object sender, PaintProcess process)
		{
			var sensor = (SynapseSensor) sender;
			sensor.Rasterization(process);
		}
	}
}
