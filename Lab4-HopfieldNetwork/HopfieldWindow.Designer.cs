﻿namespace HopfieldNetwork
{
	partial class HopfieldWindow
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
			System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
			System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HopfieldWindow));
			this.mainMenu = new System.Windows.Forms.MainMenu(this.components);
			this.menuItemFile = new System.Windows.Forms.MenuItem();
			this.menuItemNew = new System.Windows.Forms.MenuItem();
			this.menuItemSeperator1 = new System.Windows.Forms.MenuItem();
			this.menuItemOpen = new System.Windows.Forms.MenuItem();
			this.menuItemSave = new System.Windows.Forms.MenuItem();
			this.menuItemSaveAs = new System.Windows.Forms.MenuItem();
			this.menuItemSeparator2 = new System.Windows.Forms.MenuItem();
			this.menuItemExit = new System.Windows.Forms.MenuItem();
			this.menuItemAbout = new System.Windows.Forms.MenuItem();
			this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
			this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
			this.chart = new System.Windows.Forms.DataVisualization.Charting.Chart();
			this.panelButtons = new System.Windows.Forms.Panel();
			this.buttonForget = new System.Windows.Forms.Button();
			this.buttonClear = new System.Windows.Forms.Button();
			this.buttonLoadImage = new System.Windows.Forms.Button();
			this.buttonLearn = new System.Windows.Forms.Button();
			this.buttonRecognize = new System.Windows.Forms.Button();
			this.panelImages = new System.Windows.Forms.Panel();
			this.openImageDialog = new System.Windows.Forms.OpenFileDialog();
			this.panelActions = new System.Windows.Forms.Panel();
			this.panelMain = new System.Windows.Forms.Panel();
			this.synapseSensor = new NeuralNetwork.Ui.SynapseSensor();
			((System.ComponentModel.ISupportInitialize)(this.chart)).BeginInit();
			this.panelButtons.SuspendLayout();
			this.panelActions.SuspendLayout();
			this.panelMain.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.synapseSensor)).BeginInit();
			this.SuspendLayout();
			// 
			// mainMenu
			// 
			this.mainMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItemFile,
            this.menuItemAbout});
			// 
			// menuItemFile
			// 
			this.menuItemFile.Index = 0;
			this.menuItemFile.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItemNew,
            this.menuItemSeperator1,
            this.menuItemOpen,
            this.menuItemSave,
            this.menuItemSaveAs,
            this.menuItemSeparator2,
            this.menuItemExit});
			this.menuItemFile.Text = "&Файл";
			// 
			// menuItemNew
			// 
			this.menuItemNew.Index = 0;
			this.menuItemNew.Shortcut = System.Windows.Forms.Shortcut.CtrlN;
			this.menuItemNew.Text = "&Новый";
			this.menuItemNew.Click += new System.EventHandler(this.menuItemNew_Click);
			// 
			// menuItemSeperator1
			// 
			this.menuItemSeperator1.Index = 1;
			this.menuItemSeperator1.Text = "-";
			// 
			// menuItemOpen
			// 
			this.menuItemOpen.Index = 2;
			this.menuItemOpen.Shortcut = System.Windows.Forms.Shortcut.CtrlO;
			this.menuItemOpen.Text = "&Открыть";
			this.menuItemOpen.Click += new System.EventHandler(this.menuItemOpen_Click);
			// 
			// menuItemSave
			// 
			this.menuItemSave.Index = 3;
			this.menuItemSave.Shortcut = System.Windows.Forms.Shortcut.CtrlS;
			this.menuItemSave.Text = "&Сохранить";
			this.menuItemSave.Click += new System.EventHandler(this.menuItemSave_Click);
			// 
			// menuItemSaveAs
			// 
			this.menuItemSaveAs.Index = 4;
			this.menuItemSaveAs.Shortcut = System.Windows.Forms.Shortcut.CtrlShiftS;
			this.menuItemSaveAs.Text = "Сохранить &как";
			this.menuItemSaveAs.Click += new System.EventHandler(this.menuItemSaveAs_Click);
			// 
			// menuItemSeparator2
			// 
			this.menuItemSeparator2.Index = 5;
			this.menuItemSeparator2.Text = "-";
			// 
			// menuItemExit
			// 
			this.menuItemExit.Index = 6;
			this.menuItemExit.Shortcut = System.Windows.Forms.Shortcut.AltF4;
			this.menuItemExit.Text = "В&ыход";
			this.menuItemExit.Click += new System.EventHandler(this.menuItemExit_Click);
			// 
			// menuItemAbout
			// 
			this.menuItemAbout.Index = 1;
			this.menuItemAbout.Text = "&О программе";
			this.menuItemAbout.Click += new System.EventHandler(this.menuItemAbout_Click);
			// 
			// saveFileDialog
			// 
			this.saveFileDialog.DefaultExt = "nmem";
			this.saveFileDialog.FileName = "Lab4 - Neurone Memory";
			this.saveFileDialog.Filter = "Память нейрона|*.nmem|Все файлы|*.*";
			// 
			// openFileDialog
			// 
			this.openFileDialog.Filter = "Память нейрона|*.nmem|Все файлы|*.*";
			// 
			// chart
			// 
			this.chart.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			chartArea1.Name = "ChartArea";
			this.chart.ChartAreas.Add(chartArea1);
			legend1.Docking = System.Windows.Forms.DataVisualization.Charting.Docking.Bottom;
			legend1.Name = "Legend";
			this.chart.Legends.Add(legend1);
			this.chart.Location = new System.Drawing.Point(334, 0);
			this.chart.Name = "chart";
			series1.ChartArea = "ChartArea";
			series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
			series1.Legend = "Legend";
			series1.LegendText = "Энергия";
			series1.Name = "Energy";
			this.chart.Series.Add(series1);
			this.chart.Size = new System.Drawing.Size(651, 201);
			this.chart.TabIndex = 2;
			this.chart.Text = "Energy";
			// 
			// panelButtons
			// 
			this.panelButtons.Controls.Add(this.buttonForget);
			this.panelButtons.Controls.Add(this.buttonClear);
			this.panelButtons.Controls.Add(this.buttonLoadImage);
			this.panelButtons.Controls.Add(this.buttonLearn);
			this.panelButtons.Location = new System.Drawing.Point(3, 36);
			this.panelButtons.Name = "panelButtons";
			this.panelButtons.Size = new System.Drawing.Size(115, 128);
			this.panelButtons.TabIndex = 1;
			// 
			// buttonForget
			// 
			this.buttonForget.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.buttonForget.Location = new System.Drawing.Point(1, 66);
			this.buttonForget.Name = "buttonForget";
			this.buttonForget.Size = new System.Drawing.Size(114, 27);
			this.buttonForget.TabIndex = 3;
			this.buttonForget.Text = "Развидеть";
			this.buttonForget.UseVisualStyleBackColor = true;
			this.buttonForget.Click += new System.EventHandler(this.buttonForget_Click);
			// 
			// buttonClear
			// 
			this.buttonClear.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.buttonClear.Location = new System.Drawing.Point(1, 0);
			this.buttonClear.Name = "buttonClear";
			this.buttonClear.Size = new System.Drawing.Size(114, 27);
			this.buttonClear.TabIndex = 1;
			this.buttonClear.Text = "Очистить";
			this.buttonClear.UseVisualStyleBackColor = true;
			this.buttonClear.Click += new System.EventHandler(this.buttonClear_Click);
			// 
			// buttonLoadImage
			// 
			this.buttonLoadImage.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.buttonLoadImage.Location = new System.Drawing.Point(1, 99);
			this.buttonLoadImage.Name = "buttonLoadImage";
			this.buttonLoadImage.Size = new System.Drawing.Size(114, 27);
			this.buttonLoadImage.TabIndex = 4;
			this.buttonLoadImage.Text = "Загрузить";
			this.buttonLoadImage.UseVisualStyleBackColor = true;
			this.buttonLoadImage.Click += new System.EventHandler(this.buttonLoadImage_Click);
			// 
			// buttonLearn
			// 
			this.buttonLearn.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.buttonLearn.Location = new System.Drawing.Point(1, 33);
			this.buttonLearn.Name = "buttonLearn";
			this.buttonLearn.Size = new System.Drawing.Size(114, 27);
			this.buttonLearn.TabIndex = 2;
			this.buttonLearn.Text = "Увидеть";
			this.buttonLearn.UseVisualStyleBackColor = true;
			this.buttonLearn.Click += new System.EventHandler(this.buttonLearn_Click);
			// 
			// buttonRecognize
			// 
			this.buttonRecognize.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.buttonRecognize.Location = new System.Drawing.Point(4, 3);
			this.buttonRecognize.Name = "buttonRecognize";
			this.buttonRecognize.Size = new System.Drawing.Size(114, 27);
			this.buttonRecognize.TabIndex = 0;
			this.buttonRecognize.Text = "Recognize";
			this.buttonRecognize.UseVisualStyleBackColor = true;
			this.buttonRecognize.Click += new System.EventHandler(this.buttonRecognize_Click);
			// 
			// panelImages
			// 
			this.panelImages.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.panelImages.AutoScroll = true;
			this.panelImages.Location = new System.Drawing.Point(0, 207);
			this.panelImages.Name = "panelImages";
			this.panelImages.Size = new System.Drawing.Size(985, 229);
			this.panelImages.TabIndex = 3;
			// 
			// openImageDialog
			// 
			this.openImageDialog.Filter = "Картинки|*.bmp;*.jpg;*.jpeg;*.png|Все файлы|*.*";
			this.openImageDialog.Multiselect = true;
			// 
			// panelActions
			// 
			this.panelActions.Controls.Add(this.buttonRecognize);
			this.panelActions.Controls.Add(this.panelButtons);
			this.panelActions.Location = new System.Drawing.Point(207, 1);
			this.panelActions.Name = "panelActions";
			this.panelActions.Size = new System.Drawing.Size(121, 200);
			this.panelActions.TabIndex = 0;
			// 
			// panelMain
			// 
			this.panelMain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.panelMain.AutoScroll = true;
			this.panelMain.Controls.Add(this.synapseSensor);
			this.panelMain.Controls.Add(this.panelActions);
			this.panelMain.Controls.Add(this.chart);
			this.panelMain.Controls.Add(this.panelImages);
			this.panelMain.Location = new System.Drawing.Point(12, 12);
			this.panelMain.Name = "panelMain";
			this.panelMain.Size = new System.Drawing.Size(985, 523);
			this.panelMain.TabIndex = 14;
			// 
			// synapseSensor
			// 
			this.synapseSensor.BackColor = System.Drawing.Color.White;
			this.synapseSensor.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("synapseSensor.BackgroundImage")));
			this.synapseSensor.BlackConst = 1D;
			this.synapseSensor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.synapseSensor.BrushColor = System.Drawing.Color.Black;
			this.synapseSensor.ClearBrushWidth = 8;
			this.synapseSensor.Location = new System.Drawing.Point(0, 0);
			this.synapseSensor.Name = "synapseSensor";
			this.synapseSensor.PaintBrushWidth = 4;
			this.synapseSensor.Painting = true;
			this.synapseSensor.PaintOverGrid = false;
			this.synapseSensor.PixelGridColor = System.Drawing.Color.Gainsboro;
			this.synapseSensor.PixelSize = 10;
			this.synapseSensor.Size = new System.Drawing.Size(201, 201);
			this.synapseSensor.TabIndex = 13;
			this.synapseSensor.TabStop = false;
			this.synapseSensor.WhiteConst = -1D;
			this.synapseSensor.PaintEnd += new NeuralNetwork.Ui.PaintBoxPaintHandler(this.synapseSensor_PaintEnd);
			// 
			// HopfieldWindow
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1009, 547);
			this.Controls.Add(this.panelMain);
			this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.KeyPreview = true;
			this.Menu = this.mainMenu;
			this.Name = "HopfieldWindow";
			this.Text = "Hopfield network";
			this.Load += new System.EventHandler(this.HopfieldWindow_Load);
			this.DragDrop += new System.Windows.Forms.DragEventHandler(this.HopfieldWindow_DragDrop);
			this.DragEnter += new System.Windows.Forms.DragEventHandler(this.HopfieldWindow_DragEnter);
			((System.ComponentModel.ISupportInitialize)(this.chart)).EndInit();
			this.panelButtons.ResumeLayout(false);
			this.panelActions.ResumeLayout(false);
			this.panelMain.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.synapseSensor)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.MainMenu mainMenu;
		private System.Windows.Forms.MenuItem menuItemFile;
		private System.Windows.Forms.MenuItem menuItemNew;
		private System.Windows.Forms.MenuItem menuItemSeperator1;
		private System.Windows.Forms.MenuItem menuItemOpen;
		private System.Windows.Forms.MenuItem menuItemSave;
		private System.Windows.Forms.MenuItem menuItemSaveAs;
		private System.Windows.Forms.MenuItem menuItemSeparator2;
		private System.Windows.Forms.MenuItem menuItemExit;
		private System.Windows.Forms.MenuItem menuItemAbout;
		private System.Windows.Forms.SaveFileDialog saveFileDialog;
		private System.Windows.Forms.OpenFileDialog openFileDialog;
		private NeuralNetwork.Ui.SynapseSensor synapseSensor;
		private System.Windows.Forms.Button buttonLearn;
		private System.Windows.Forms.Button buttonClear;
		private System.Windows.Forms.Button buttonRecognize;
		private System.Windows.Forms.Panel panelImages;
		private System.Windows.Forms.OpenFileDialog openImageDialog;
		private System.Windows.Forms.Button buttonLoadImage;
		private System.Windows.Forms.Button buttonForget;
		private System.Windows.Forms.Panel panelButtons;
		private System.Windows.Forms.DataVisualization.Charting.Chart chart;
		private System.Windows.Forms.Panel panelActions;
		private System.Windows.Forms.Panel panelMain;
	}
}

