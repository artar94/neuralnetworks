﻿using System;
using System.Collections.Generic;
using System.Linq;
using NeuralNetwork;

namespace HopfieldNetwork
{
	public sealed class Hopfield : NeuralNetwork.NeuralNetwork
	{
		public double ForgetSpeed { get; set; } = 0.1;

		public List<double[]> Samples { get; set; }

		public override int NeuroneCount
		{
			get { return Neurones.Count; }
			set
			{
				if (Neurones == null)
					Neurones = new List<Neurone>(value);

				for (var i = Neurones.Count; i < value; i++)
				{
					Neurones.Add(new Neurone(false));
				}

				Neurones.RemoveRange(value, Neurones.Count - value);
			}
		}

		public override IEnumerable<double> Recognize(ISynapse<double>[] synapses)
		{
			throw new NotSupportedException();
		}

		public IEnumerable<ISynapse<double>[]> Recovering(ISynapse<double>[] synapses)
		{
			NeuroneCount = synapses.Length;
			
			while (true)
			{
				var origin = synapses.Select(s => (ISynapse<double>) new Synapse(s.Value)).ToArray();
				Neurones.ForEach(n => n.Synapses = origin);

				for (var i = 0; i < synapses.Length; i++)
				{
					var result = Neurones[i].Recognize();
					if (Math.Abs(result) > 0.0001)
						synapses[i].Value = result;

					yield return synapses;
				}

				if (synapses.SequenceEqual(origin))
					break;
			}
		}

		public override void Learn(ISynapse<double>[] synapses, IList<double> answers)
		{
			Training(synapses, 1);
		}

		public void Forget(ISynapse<double>[] synapses)
		{
			Training(synapses, -ForgetSpeed);
		}

		private void Training(ISynapse<double>[] synapses, double speed)
		{
			NeuroneCount = synapses.Length;
			Neurones.ForEach(n => n.Synapses = synapses);

			for (var i = 0; i < synapses.Length; i++)
			{
				var weights = new double[synapses.Length];
				for (var j = 0; j < synapses.Length; j++)
				{
					weights[j] = i == j ? 0 : (Neurones[i].SynapseWeights[j] + speed*synapses[i].Value*synapses[j].Value);
				}
				Neurones[i].SynapseWeights = weights;
			}

			OnTrained();
		}
	}
}