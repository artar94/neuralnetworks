﻿using System.Collections.Generic;
using NeuralNetwork;
using NeuralNetwork.Ui;

namespace HopfieldNetwork
{
	public class Document: BaseDocument
	{
		public Hopfield NeuralNetwork { get; }

		public Document()
		{
			NeuralNetwork = new Hopfield
			{
				NeuroneCount = 0,
				Samples = new List<double[]>(),
			};
		}

		public Document(string fileName)
		{
			var trainer = new JsonTrainer<Hopfield>();
			NeuralNetwork = trainer.LoadFromFile(fileName);
			if (NeuralNetwork.Samples == null)
				NeuralNetwork.Samples = new List<double[]>();

			FileName = fileName;
		}

		public override bool IsModified => false;

		public override void SaveToFile(string fileName)
		{
			var trainer = new JsonTrainer<Hopfield>();
			trainer.SaveToFile(fileName, NeuralNetwork);
			base.SaveToFile(fileName);
		}

		public IEnumerable<double> Recognize(ISynapse<double>[] synapses)
		{
			return NeuralNetwork.Recognize(synapses);
		}

		public void Learn(ISynapse<double>[] synapses)
		{
			NeuralNetwork.Learn(synapses, null);
		}

		public void Forget(ISynapse<double>[] synapses)
		{
			NeuralNetwork.Forget(synapses);
		}
	}
}