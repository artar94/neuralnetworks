﻿using System;
using System.Collections.Generic;
using System.Linq;
using NeuralNetwork;

namespace ThreeNeurone
{
	public sealed class RosenblattPerceptron: NeuralNetwork.NeuralNetwork
	{
		public override int NeuroneCount
		{
			get { return Neurones.Count; }
			set
			{
				if (Neurones == null)
					Neurones = new List<Neurone>(value);

				for (var i = Neurones.Count; i < value; i++)
				{
					Neurones.Add(new Neurone());
				}

				Neurones.RemoveRange(value, Neurones.Count - value);
			}
		}

		public override IEnumerable<double> Recognize(ISynapse<double>[] synapses)
		{
			Neurones.ForEach(n => n.Synapses = synapses);

			return Neurones.Select(n => n.Recognize()).ToList();
		}

		public override void Learn(ISynapse<double>[] synapses, IList<double> answers)
		{
			Neurones.ForEach(n => n.Synapses = synapses);

			for (var i = 0; i < Math.Min(Neurones.Count, answers.Count); i++)
			{
				if (Math.Sign(Neurones[i].Recognize()) == Math.Sign(answers[i])) continue;

				Neurones[i].Learn(answers[i]);
				Neurones[i].LearnSpead *= 0.95;
			}

			OnTrained();
		}
	}
}