﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using NeuralNetwork;
using NeuralNetwork.Ui;

namespace ThreeNeurone
{
	public enum Symbol
	{
		[Description("Неизв.")]
		Unknown,

		[Description("A")]
		A,

		[Description("B")]
		B,

		[Description("C")]
		C,
	}

	public class Document: BaseDocument
	{
		private const int NeuroneCount = 3;
		public RosenblattPerceptron NeuralNetwork { get; }

		public Document()
		{
			NeuralNetwork = new RosenblattPerceptron()
			{
				NeuroneCount = NeuroneCount,
			};
		}

		public Document(string fileName)
		{
			var trainer = new JsonTrainer<RosenblattPerceptron>();
			NeuralNetwork = trainer.LoadFromFile(fileName);
			NeuralNetwork.NeuroneCount = NeuroneCount;

			FileName = fileName;
		}

		public override bool IsModified => false;

		public override void SaveToFile(string fileName)
		{
			var trainer = new JsonTrainer<RosenblattPerceptron>();
			trainer.SaveToFile(fileName, NeuralNetwork);
			base.SaveToFile(fileName);
		}

		private static Symbol IndexToSymbol(int value)
		{
			switch (value)
			{
				case 0: return Symbol.A;
				case 1: return Symbol.B;
				case 2: return Symbol.C;
				default:
					return Symbol.Unknown;
			}
		}

		private static int SymbolToIndex(Symbol value)
		{
			switch (value)
			{
				case Symbol.A: return 0;
				case Symbol.B: return 1;
				case Symbol.C: return 2;
				default:
					return -1;
			}
		}

		public Symbol Recognize(ISynapse<double>[] synapses, out IList<double> results)
		{
			results = NeuralNetwork.Recognize(synapses).ToList();

			if (results.Count(d => Math.Sign(d) == 1) > 1)
				return Symbol.Unknown;

			if (results.Any(r => r > 0))
			{
				var maxIndex = results.IndexOf(results.Max());
				return IndexToSymbol(maxIndex);
			}
			return Symbol.Unknown;
		}

		public void Learn(ISynapse<double>[] synapses, Symbol symbol)
		{
			var answers = new[] {-1.0, -1.0, -1.0};
			var index = SymbolToIndex(symbol);
			if (index >= 0)
				answers[index] = 1.0;

			NeuralNetwork.Learn(synapses, answers);
		}
	}
}