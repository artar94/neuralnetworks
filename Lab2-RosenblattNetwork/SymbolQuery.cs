﻿using System;
using System.Windows.Forms;
using NeuralNetwork.Extensions;

namespace ThreeNeurone
{
	public partial class SymbolQuery : Form
	{
		public SymbolQuery()
		{
			InitializeComponent();
		}

		public Symbol ShowSymbolDialog(IWin32Window owner, Symbol symbol = Symbol.Unknown)
		{
			radioButton1.Text = $"Это {Symbol.A.GetDescription()}";
			radioButton2.Text = $"Это {Symbol.B.GetDescription()}";
			radioButton3.Text = $"Это {Symbol.C.GetDescription()}";

			switch (symbol)
			{
				case Symbol.A:
					radioButton1.Checked = true;
					break;
				case Symbol.B:
					radioButton2.Checked = true;
					break;
				case Symbol.C:
					radioButton3.Checked = true;
					break;
				default:
					radioButton1.Checked = false;
					break;
			}

			if (ShowDialog(owner) == DialogResult.OK)
			{
				if (radioButton1.Checked) return Symbol.A;
				if (radioButton2.Checked) return Symbol.B;
				if (radioButton3.Checked) return Symbol.C;

				return Symbol.Unknown;
			}
			return Symbol.Unknown;
		}

		public Symbol ShowSymbolDialog(Symbol symbol = Symbol.Unknown)
		{
			return ShowSymbolDialog(null, symbol);
		}

		private void buttonOk_Click(object sender, EventArgs e)
		{
			DialogResult = DialogResult.OK;
		}
	}
}
