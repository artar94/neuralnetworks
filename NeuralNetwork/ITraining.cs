﻿namespace NeuralNetwork
{
	public delegate void WeightChangedEvent(object sender);

	public interface ITraining
	{
		double[] SynapseWeights { get; set; }
		double InternalWeight { get; set; }

		event WeightChangedEvent WeightChanged;
	}
}