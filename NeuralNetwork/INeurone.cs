﻿namespace NeuralNetwork
{
	public interface INeurone<TInput, out TResult>
	{
		ISynapse<TInput>[] Synapses { get; set; }
		TResult Recognize();
	}
}