﻿using System;
using System.ComponentModel;
using System.Linq;

namespace NeuralNetwork.Extensions
{
	public static class EnumExtension
	{
		public static string GetDescription(this Enum value)
		{
			var fieldInfo = value.GetType().GetField(value.ToString());
			if (fieldInfo == null)
				return value.ToString();
			var description = fieldInfo.GetCustomAttributes(typeof (DescriptionAttribute), false)
				.Cast<DescriptionAttribute>()
				.SingleOrDefault();

			return description == null ? value.ToString() : description.Description;
		}
	}
}