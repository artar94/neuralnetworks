﻿using CLRRandom = System.Random;

namespace NeuralNetwork
{
	public static class RandomStatic
	{
		private static CLRRandom _rand;

		private static CLRRandom Rand => _rand ?? (_rand = new CLRRandom());

		//
		// Summary:
		//     Returns a nonnegative random integer.
		//
		// Returns:
		//     A 32-bit signed integer greater than or equal to zero and less than System.Int32.MaxValue.
		public static int Next()
		{
			return Rand.Next();
		}

		//
		// Summary:
		//     Returns a nonnegative random integer that is less than the specified maximum.
		//
		// Parameters:
		//   maxValue:
		//     The exclusive upper bound of the random number to be generated. maxValue must
		//     be greater than or equal to zero.
		//
		// Returns:
		//     A 32-bit signed integer greater than or equal to zero, and less than maxValue;
		//     that is, the range of return values ordinarily includes zero but not maxValue.
		//     However, if maxValue equals zero, maxValue is returned.
		//
		// Exceptions:
		//   T:System.ArgumentOutOfRangeException:
		//     maxValue is less than zero.
		public static int Next(int maxValue)
		{
			return Rand.Next(maxValue);
		}

		//
		// Summary:
		//     Returns a random integer that is within a specified range.
		//
		// Parameters:
		//   minValue:
		//     The inclusive lower bound of the random number returned.
		//
		//   maxValue:
		//     The exclusive upper bound of the random number returned. maxValue must be greater
		//     than or equal to minValue.
		//
		// Returns:
		//     A 32-bit signed integer greater than or equal to minValue and less than maxValue;
		//     that is, the range of return values includes minValue but not maxValue. If minValue
		//     equals maxValue, minValue is returned.
		//
		// Exceptions:
		//   T:System.ArgumentOutOfRangeException:
		//     minValue is greater than maxValue.
		public static int Next(int minValue, int maxValue)
		{
			return Rand.Next(minValue, maxValue);
		}

		//
		// Summary:
		//     Fills the elements of a specified array of bytes with random numbers.
		//
		// Parameters:
		//   buffer:
		//     An array of bytes to contain random numbers.
		//
		// Exceptions:
		//   T:System.ArgumentNullException:
		//     buffer is null.
		public static void NextBytes(byte[] buffer)
		{
			Rand.NextBytes(buffer);
		}

		//
		// Summary:
		//     Returns a random floating-point number between 0.0 and 1.0.
		//
		// Returns:
		//     A double-precision floating point number greater than or equal to 0.0, and less
		//     than 1.0.
		public static double NextDouble()
		{
			return Rand.NextDouble();
		}
	}
}