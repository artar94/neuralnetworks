﻿using System.IO;
using Newtonsoft.Json;

namespace NeuralNetwork
{
	public class JsonTrainer<T> where T : class
	{
		public void SaveToFile(string fileName, T neuralNetwork)
		{
			File.Create(fileName).Close();

			using (var r = new StreamWriter(fileName))
			{
				var json = JsonConvert.SerializeObject(neuralNetwork);
				r.Write(json);
			}
		}

		public T LoadFromFile(string fileName)
		{
			using (var r = new StreamReader(fileName))
			{
				var json = r.ReadToEnd();
				return JsonConvert.DeserializeObject<T>(json);
			}
		}
	}
}