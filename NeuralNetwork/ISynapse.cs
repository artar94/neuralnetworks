﻿using System;

namespace NeuralNetwork
{
	public interface ISynapse<TInput>: IEquatable<ISynapse<TInput>>
	{
		TInput Value { get; set; }
	}
}