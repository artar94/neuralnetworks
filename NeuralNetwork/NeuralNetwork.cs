﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace NeuralNetwork
{
	public delegate void TrainedEvent(object sender);

	public abstract class NeuralNetwork
	{
		public virtual List<Neurone> Neurones { get; set; }

		public event TrainedEvent Trained;

		[JsonIgnore]
		public abstract int NeuroneCount { get; set; }

		public abstract IEnumerable<double> Recognize(ISynapse<double>[] synapses);

		public abstract void Learn(ISynapse<double>[] synapses, IList<double> answers);

		protected virtual void OnTrained()
		{
			Trained?.Invoke(this);
		}
	}
}