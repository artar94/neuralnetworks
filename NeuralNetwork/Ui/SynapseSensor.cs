﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Windows.Forms;

namespace NeuralNetwork.Ui
{
	public class SynapseSensor : PaintPox
	{
		private const double PaintingRatio = 0.15;
		private const double CleaningRatio = 0.9;

		private int _pixelSize = 1;
		private Color _pixelGridColor = Color.Gainsboro;
		private bool _paintOverGrid = true;

		private readonly object _lockImageObject = new object();

		public int PixelSize
		{
			get { return _pixelSize; }
			set
			{
				_pixelSize = value;
				if (_pixelSize < 1) _pixelSize = 1;

				PaintPixelGrid();
			}
		}

		public Color PixelGridColor
		{
			get { return _pixelGridColor; }
			set
			{
				_pixelGridColor = value;
				PaintPixelGrid();
			}
		}

		public bool PaintOverGrid
		{
			get { return _paintOverGrid; }
			set
			{
				_paintOverGrid = value;
				PaintPixelGrid();
			}
		}

		public double BlackConst { get; set; } = 1;
		public double WhiteConst { get; set; } = 0;

		public int HorizontalPixelCount => (int) Math.Ceiling((double) ClientSize.Width/PixelSize);
		public int VerticalPixelCount => (int) Math.Ceiling((double) ClientSize.Height/PixelSize);

		public SynapseSensor()
		{
			PaintPixelGrid();
		}

		private static bool IsBlackPixel(Color color)
		{
			return (color.A > 128) && (color.GetBrightness() <= 0.5);
		}

		private static bool IsBlackRegion(Bitmap bmp, int x, int y, int size, double ratio)
		{
			var blackCount = 0;
			var whiteCount = 0;
			for (var i = x; (i < x + size) && (i < bmp.Width); i++)
			{
				for (var j = y; (j < y + size) && (j < bmp.Height); j++)
				{
					if (IsBlackPixel(bmp.GetPixel(i, j)))
						blackCount++;
					else
						whiteCount++;
				}
			}
			return blackCount*1.0/(blackCount + whiteCount) > ratio;
		}

		private bool IsPixelScaled => !PaintOverGrid && PixelSize > 1;

		private ISynapse<double>[] GetSynapses(PaintProcess process)
		{
			lock (_lockImageObject)
			{
				var result = new List<ISynapse<double>>();
				if (Image == null) return result.ToArray();

				var bmp = (Bitmap) Image;
				for (var j = 0; j < bmp.Height; j += PixelSize)
				{
					for (var i = 0; i < bmp.Width; i += PixelSize)
					{
						var isBlack = IsBlackRegion(bmp,
							i, j,
							IsPixelScaled ? PixelSize - 1 : PixelSize,
							process == PaintProcess.Painting ? PaintingRatio : CleaningRatio);
						result.Add(new Synapse(isBlack ? BlackConst : WhiteConst));
					}
				}

				return result.ToArray();
			}
		}

		[Browsable(false), EditorBrowsable(EditorBrowsableState.Never)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public ISynapse<double>[] Synapses
		{
			get { return GetSynapses(PaintProcess.Painting); }
			set { DrawSynapses(value); }
		}

		public int SynapseCount => (Image?.Height/PixelSize)*(Image?.Width/PixelSize) ?? 0;

		public Rectangle GetFigureRectangle()
		{
			if (Image == null)
				return Rectangle.Empty;

			var left = Image.Width;
			var right = 0;
			var top = Image.Height;
			var bottom = 0;

			var bmp = (Bitmap) Image;

			for (var i = 0; i < Image.Height; i++)
			{
				for (var j = 0; j < Image.Width; j++)
				{
					if (IsBlackPixel(bmp.GetPixel(j, i)))
					{
						if (i < top) top = i;
						if (i > bottom) bottom = i;

						if (j < left) left = j;
						if (j > right) right = j;
					}
				}
			}

			if ((left >= right) || (top >= bottom))
				return Rectangle.Empty;

			return new Rectangle(left, top, right - left + 1, bottom - top + 1);
		}

		public Bitmap GetFigureStretched()
		{
			if (Image == null)
				return new Bitmap(0, 0);

			var sourceRect = GetFigureRectangle();

			var destRect = new Rectangle(0, 0, Image.Width, Image.Height);
			var destImage = new Bitmap(Image.Width, Image.Height);

			destImage.SetResolution(Image.HorizontalResolution, Image.VerticalResolution);

			using (var graphics = Graphics.FromImage(destImage))
			{
				graphics.Clear(Color.Transparent);

				graphics.CompositingMode = CompositingMode.SourceCopy;
				graphics.CompositingQuality = CompositingQuality.HighQuality;
				graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
				graphics.SmoothingMode = SmoothingMode.HighQuality;
				graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

				using (var wrapMode = new ImageAttributes())
				{
					wrapMode.SetWrapMode(WrapMode.TileFlipXY);
					graphics.DrawImage(Image, destRect, sourceRect.X, sourceRect.Y, sourceRect.Width, sourceRect.Height,
						GraphicsUnit.Pixel, wrapMode);
				}
			}

			return destImage;
		}

		private void DrawSynapses(ISynapse<double>[] synapses)
		{
			lock (_lockImageObject)
			{
				var bmp = new Bitmap(ClientSize.Width, ClientSize.Height);
				using (var g = Graphics.FromImage(bmp))
				{
					g.Clear(Color.Transparent);

					var x = 0;
					var y = 0;
					for (var i = 0; i < synapses.Length; i++)
					{
						if (i >= ClientSize.Width*ClientSize.Height)
							break;

						if (Math.Abs(synapses[i].Value - BlackConst) < 0.0001)
							g.FillRectangle(Brushes.Black,
								x, y,
								IsPixelScaled ? PixelSize - 1 : PixelSize,
								IsPixelScaled ? PixelSize - 1 : PixelSize);

						x += PixelSize;
						if (x >= bmp.Width)
						{
							x = 0;
							y += PixelSize;
						}
					}
				}
				Image = bmp;
			}
		}

		public void Stretch()
		{
			if (Image == null)
				return;

			Image = GetFigureStretched();
		}

		public void Rasterization(PaintProcess process = PaintProcess.Painting)
		{
			DrawSynapses(GetSynapses(process));
		}

		public void Rasterization(Image image, PaintProcess process = PaintProcess.Painting)
		{
			lock (_lockImageObject)
			{
				var destRect = new Rectangle(0, 0, ClientSize.Width, ClientSize.Height);
				var destImage = new Bitmap(ClientSize.Width, ClientSize.Height);

				destImage.SetResolution(Image.HorizontalResolution, Image.VerticalResolution);
				using (var graphics = Graphics.FromImage(destImage))
				{
					graphics.Clear(Color.Transparent);

					graphics.CompositingMode = CompositingMode.SourceCopy;
					graphics.CompositingQuality = CompositingQuality.HighQuality;
					graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
					graphics.SmoothingMode = SmoothingMode.HighQuality;
					graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

					using (var wrapMode = new ImageAttributes())
					{
						wrapMode.SetWrapMode(WrapMode.TileFlipXY);
						graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);
					}
				}
				Image = destImage;
			}

			Rasterization(process);
		}


		private void PaintPixelGrid()
		{
			var bmp = new Bitmap(ClientSize.Width, ClientSize.Height);
			using (var g = Graphics.FromImage(bmp))
			{
				g.Clear(Color.Transparent);

				if (PixelSize > 1)
				{
					var pen = new Pen(PixelGridColor, 1);
					for (var i = PixelSize - 1; i < ClientSize.Width; i += PixelSize)
					{
						g.DrawLine(pen, i, 0, i, ClientSize.Height);
					}

					for (var i = PixelSize - 1; i < ClientSize.Height; i += PixelSize)
					{
						g.DrawLine(pen, 0, i, ClientSize.Width, i);
					}
				}
			}

			BackgroundImage = bmp;
			BackgroundImageLayout = ImageLayout.Tile;
		}

		protected override void OnResize(EventArgs e)
		{
			base.OnResize(e);

			PaintPixelGrid();
		}
	}
}