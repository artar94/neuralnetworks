﻿namespace NeuralNetwork.Ui
{
	public abstract class BaseDocument: IDocument
	{
		public string FileName { get; protected set; } = "";
		public abstract bool IsModified { get; }

		public virtual void SaveToFile(string fileName)
		{
			FileName = fileName;
			Saved?.Invoke(this, fileName);
		}

		public event DocumentSavedEvent Saved;
	}
}