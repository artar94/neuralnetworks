﻿namespace NeuralNetwork.Ui
{
	partial class NeuroneMonitor
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.panel = new System.Windows.Forms.Panel();
			this.LabelInternalWeight = new System.Windows.Forms.Label();
			this.PictureBoxWeights = new System.Windows.Forms.PictureBox();
			this.panel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.PictureBoxWeights)).BeginInit();
			this.SuspendLayout();
			// 
			// panel
			// 
			this.panel.Controls.Add(this.LabelInternalWeight);
			this.panel.Controls.Add(this.PictureBoxWeights);
			this.panel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel.Location = new System.Drawing.Point(0, 0);
			this.panel.Margin = new System.Windows.Forms.Padding(0);
			this.panel.Name = "panel";
			this.panel.Size = new System.Drawing.Size(100, 121);
			this.panel.TabIndex = 3;
			// 
			// LabelInternalWeight
			// 
			this.LabelInternalWeight.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.LabelInternalWeight.AutoSize = true;
			this.LabelInternalWeight.Location = new System.Drawing.Point(3, 101);
			this.LabelInternalWeight.Name = "LabelInternalWeight";
			this.LabelInternalWeight.Size = new System.Drawing.Size(63, 13);
			this.LabelInternalWeight.TabIndex = 4;
			this.LabelInternalWeight.Text = "Внутр. вес:";
			// 
			// PictureBoxWeights
			// 
			this.PictureBoxWeights.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.PictureBoxWeights.BackColor = System.Drawing.Color.White;
			this.PictureBoxWeights.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.PictureBoxWeights.Location = new System.Drawing.Point(0, 0);
			this.PictureBoxWeights.Name = "PictureBoxWeights";
			this.PictureBoxWeights.Size = new System.Drawing.Size(100, 98);
			this.PictureBoxWeights.TabIndex = 3;
			this.PictureBoxWeights.TabStop = false;
			// 
			// NeuroneMonitor
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.panel);
			this.Name = "NeuroneMonitor";
			this.Size = new System.Drawing.Size(100, 121);
			this.panel.ResumeLayout(false);
			this.panel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.PictureBoxWeights)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Panel panel;
		public System.Windows.Forms.Label LabelInternalWeight;
		public System.Windows.Forms.PictureBox PictureBoxWeights;
	}
}
