﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace NeuralNetwork.Ui
{
	public partial class NeuroneMonitor : UserControl
	{
		public NeuroneMonitor()
		{
			InitializeComponent();
		}

		private ITraining _neurone;
		private int _pixelSize = 1;

		[Browsable(false), EditorBrowsable(EditorBrowsableState.Never)]
		public ITraining Neurone
		{
			get
			{
				return _neurone;
			}
			set
			{
				_neurone = value;

				Clean();
				if (_neurone == null)
					return;

				_neurone.WeightChanged += sender => ApplyMemory();

				ApplyMemory();
			}
		}

		public int PixelSize
		{
			get { return _pixelSize; }
			set
			{
				_pixelSize = value;

				if (_neurone == null)
					return;

				PaintMemory();
			}
		}

		public void Clean()
		{
			if (PictureBoxWeights.Image == null) return;

			var bmp = new Bitmap(PictureBoxWeights.ClientSize.Width, PictureBoxWeights.ClientSize.Height);
			using (var g = Graphics.FromImage(bmp))
			{
				g.Clear(BackColor);
			}
			PictureBoxWeights.Image = bmp;

			PictureBoxWeights.Invalidate();

			LabelInternalWeight.Text = @"Внут. вес: 0";
		}

		private void ApplyMemory()
		{
			PaintMemory();
			LabelInternalWeight.Text = $"Внут. вес: {_neurone.InternalWeight:0.##}";
		}

		private void PaintMemory()
		{
			var bmp = new Bitmap(PictureBoxWeights.ClientSize.Width, PictureBoxWeights.ClientSize.Height);
			using (var g = Graphics.FromImage(bmp))
			{
				g.Clear(PictureBoxWeights.BackColor);

				if (_neurone.SynapseWeights != null)
				{
					var x = 0;
					var y = 0;
					for (var i = 0; i < _neurone.SynapseWeights.Length; i++)
					{
						if (i >= bmp.Width* bmp.Height)
							break;

						var color = AdjustBrightness(_neurone.SynapseWeights[i] > 0 ? Color.Red : Color.Green, Math.Abs(_neurone.SynapseWeights[i]));

						g.FillRectangle(new SolidBrush(color), x, y, PixelSize, PixelSize);
						x += PixelSize;
						if (x >= bmp.Width)
						{
							x = 0;
							y += PixelSize;
						}
					}

					PictureBoxWeights.Invalidate();
				}
			}
			PictureBoxWeights.Image = bmp;
		}

		private static Color AdjustBrightness(Color color, double factor)
		{
			var r = ((color.R * factor) > 255) ? 255 : (color.R * factor);
			var g = ((color.G * factor) > 255) ? 255 : (color.G * factor);
			var b = ((color.B * factor) > 255) ? 255 : (color.B * factor);

			return Color.FromArgb(color.A, (int)r, (int)g, (int)b);
		}
	}
}
