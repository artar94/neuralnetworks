﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using NeuralNetwork.Extensions;

namespace NeuralNetwork.Ui
{
	public enum PaintProcess
	{
		Painting,
		Cleaning,
	}

	public delegate void PaintBoxPaintHandler(object sender, PaintProcess process);

	public class PaintPox: PictureBox
	{
		public bool Painting { get; set; } = true;

		public Color BrushColor { get; set; } = Color.Black;
		public int PaintBrushWidth { get; set; } = 3;
		public int ClearBrushWidth { get; set; } = 3;

		[Browsable(true)]
		public event PaintBoxPaintHandler PaintStart;

		[Browsable(true)]
		public event PaintBoxPaintHandler PaintEnd;

		public PaintPox()
		{
			BorderStyle = BorderStyle.FixedSingle;
		}

		public void Clean()
		{
			if (Image == null) return;

			var bmp = new Bitmap(ClientSize.Width, ClientSize.Height);
			using (var g = Graphics.FromImage(bmp))
			{
				g.Clear(Color.Transparent);
			}
			if (!DesignMode)
				Image = bmp;

			Invalidate();
		}

		private bool _mouseDown;
		private bool _mouseEnter;
		private Point _previous;
		private PaintProcess _paintProcess;

		protected override void OnMouseDown(MouseEventArgs e)
		{
			base.OnMouseDown(e);

			_mouseDown = true;
			_previous = e.Location;
			_paintProcess = e.Button == MouseButtons.Right ? PaintProcess.Cleaning : PaintProcess.Painting;

			OnMouseMove(e);
			OnPaintStart(_paintProcess);
		}

		protected override void OnMouseUp(MouseEventArgs e)
		{
			base.OnMouseUp(e);

			_mouseDown = false;
			OnPaintEnd(_paintProcess);
		}

		protected override void OnMouseEnter(EventArgs e)
		{
			base.OnMouseEnter(e);

			_previous = PointToClient(MousePosition);
			_mouseEnter = true;
		}

		protected override void OnMouseLeave(EventArgs e)
		{
			base.OnMouseLeave(e);

			_mouseEnter = false;
		}

		protected override void OnMouseMove(MouseEventArgs e)
		{
			base.OnMouseMove(e);

			if (!_mouseDown || !_mouseEnter || !Painting) return;

			if (!DesignMode)
			{
				if (Image == null)
				{
					var bmp = new Bitmap(ClientSize.Width, ClientSize.Height);
					using (var g = Graphics.FromImage(bmp))
					{
						g.Clear(Color.Transparent);
					}

					Image = bmp;
				}

				using (var g = Graphics.FromImage(Image))
				{
					var brush = new SolidBrush(_paintProcess == PaintProcess.Painting ? BrushColor : Color.Transparent);
					var brushWidth = _paintProcess == PaintProcess.Painting ? PaintBrushWidth : ClearBrushWidth;

					g.SmoothingMode = _paintProcess == PaintProcess.Painting ? SmoothingMode.AntiAlias : SmoothingMode.None;
					g.CompositingMode = _paintProcess == PaintProcess.Painting ? CompositingMode.SourceOver : CompositingMode.SourceCopy;

					g.DrawLine(new Pen(brush, brushWidth), _previous, e.Location);
					g.FillCircle(brush, e.Location.X, e.Location.Y, brushWidth/2.0f);
				}
			}
			Invalidate();
			_previous = e.Location;
		}

		protected override void OnResize(EventArgs e)
		{
			base.OnResize(e);

			if (!DesignMode)
			{
				var bmp = new Bitmap(ClientSize.Width, ClientSize.Height);
				using (var g = Graphics.FromImage(bmp))
				{
					g.Clear(Color.Transparent);
					if (Image != null)
						g.DrawImage(Image, new Point());
				}

				Image = bmp;
			}
		}

		protected virtual void OnPaintStart(PaintProcess process)
		{
			PaintStart?.Invoke(this, process);
		}

		protected virtual void OnPaintEnd(PaintProcess process)
		{
			PaintEnd?.Invoke(this, process);
		}
	}
}