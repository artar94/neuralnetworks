﻿namespace NeuralNetwork.Ui
{
	public delegate void DocumentSavedEvent(object sender, string fileName);

	public interface IDocument
	{
		string FileName { get; }
		bool IsModified { get; }

		void SaveToFile(string fileName);

		event DocumentSavedEvent Saved;
	}
}