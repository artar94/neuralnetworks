﻿using System;

namespace NeuralNetwork
{
	public class Synapse : ISynapse<double>
	{
		public double Value { get; set; }

		public Synapse(double value)
		{
			Value = value;
		}

		public bool Equals(ISynapse<double> other)
		{
			if (other == null)
				return false;

			return Math.Abs(Value - other.Value) < 0.0001;
		}
	}
}