﻿namespace NeuralNetwork
{
	public interface ILearning<in TInput>
	{
		void Learn(TInput subject);
	}
}