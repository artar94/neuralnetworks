﻿using System;
using System.Linq;
using Newtonsoft.Json;

namespace NeuralNetwork
{
	public class Neurone: INeurone<double, double>, ILearning<double>, ITraining
	{
		public double LearnSpead { get; set; } = 0.5;

		private ISynapse<double>[] _synapses = new ISynapse<double>[0];
		private double[] _weights = new double[0];

		private readonly bool _randomInit;

		[JsonIgnore]
		public ISynapse<double>[] Synapses
		{
			get { return _synapses; }
			set
			{
				_synapses = value??new ISynapse<double>[0];
				if (_synapses.Length != _weights.Length)
				{
					SynapseWeights = GetAlignedWeight(ref _weights);
				}
			}
		}

		public double[] SynapseWeights
		{
			get
			{
				return _synapses.Length != _weights.Length ? GetAlignedWeight(ref _weights) : _weights;
			}
			set
			{
				_weights = value??new double[0];
				WeightChanged?.Invoke(this);
			}
		}

		public double InternalWeight { get; set; }

		public event WeightChangedEvent WeightChanged;

		public Neurone(bool randomInit = true)
		{
			_randomInit = randomInit;
			InternalWeight = _randomInit ? RandomStatic.NextDouble() - 0.5 : 0.0;
		}

		public double Recognize()
		{
			var result = Synapses.Select((t, i) => t.Value*SynapseWeights[i]).Sum() + InternalWeight;
			return Math.Sign(result);
		}

		public void Learn(double value)
		{
			var recognize = Recognize();

			var error = value - recognize;
			for (var i = 0; i < Synapses.Length; i++)
			{
				SynapseWeights[i] += LearnSpead*error*Synapses[i].Value;
			}
			InternalWeight += LearnSpead*error;

			WeightChanged?.Invoke(this);
		}

		private double[] GetAlignedWeight(ref double[] weights)
		{
			var synapsesArray = _synapses ?? new ISynapse<double>[0];
			var weightsArray = weights ?? new double[0];

			if (synapsesArray.Length == weightsArray.Length)
				return weights;

			var dest = new double[synapsesArray.Length];
			Array.Copy(weightsArray, dest, Math.Min(weightsArray.Length, dest.Length));
			for (var i = weightsArray.Length; i < dest.Length; i++)
			{
				dest[i] = _randomInit ? RandomStatic.NextDouble() - 0.5 : 0.0;
			}

			return dest;
		}
	}
}